#include<stdio.h>
int main()
{
    int ar[10][10],m,n;
    
    printf("Enter rows and columns ");
    scanf("%d%d",&m,&n);
    
    printf("\nEnter array ");
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            scanf("%d",&ar[i][j]);
        }
    }
    
    printf("\nArray before transpose is \n\n");     
    for(int i=0;i<m;i++)
    {
        for(int j=0;j<n;j++)
        {
            printf("\t%d",ar[i][j]);
        }
        printf("\n\n");
    }
    
    printf("\nTranspose of array is \n\n");
    for(int j=0;j<n;j++)
    {
        for(int i=0;i<m;i++)
        {
            printf("\t%d",ar[i][j]);
        }
        printf("\n\n");
    }
    
    return 0;
}
