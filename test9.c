#include<stdio.h>
struct student
{
    int rno;
    char sname[15];
    char sec[3];
    char dept[20];
    int fees;
    int res;
};

void input(struct student s1, struct student s2);
void output(struct student s1, struct student s2);

int main()
{
    struct student s1,s2;
    input(s1,s2);
    output(s1,s2);
    return 0;
}

void input(struct student s1, struct student s2)                                                                   
{
    printf("\nEnter details (roll no., name, section, dept, fees, result) \n\n");
    
    printf("\nEnter details of student 1 \n");
    scanf("%d%s%s%s%d%d",&s1.rno,s1.sname,s1.sec,s1.dept,&s1.fees,&s1.res);
    
    printf("\nEnter details of student 2 \n");
    scanf("%d%s%s%s%d%d",&s2.rno,s2.sname,s2.sec,s2.dept,&s2.fees,&s2.res);
}

void output(struct student s1, struct student s2)
{
    if(s1.res>s2.res)
    {
        printf("\nRoll no. : %d \nName : %s \nSection : %s \nDept. : %s \nFees : %d \nResult : %d \n",
        s1.rno,s1.sname,s1.sec,s1.dept,s1.fees,s1.res);
    }
    else
    {
        printf("\nRoll no. : %d \nName : %s \nSection : %s \nDept. : %s \nFees : %d \nResult : %d \n",
        s2.rno,s2.sname,s2.sec,s2.dept,s2.fees,s2.res);
    }
}

