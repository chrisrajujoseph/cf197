#include<stdio.h>

char str[10];
void input();
int compute();
void output(int a);

int main()
{
	input();
	int m=compute();
	output(m);
}

void input()
{
	printf("Enter the string \n");                         
	scanf("%s",str);
}

int compute()
{
	int i=0,k=0;
	while (str[i]!='\0') 
	{
		i++;
	}
	int u=i-1;
	int l=0;
	
	while(u>l)
	{
		if(str[l++]!=str[u--])
		{
			k=1;
			break;
		}
	}
	return k;
}

void output(int a)
{
	if (a==1)
	printf("%s is not a palindrome \n",str);
	else
	printf("%s is a palindrome \n",str);
}


