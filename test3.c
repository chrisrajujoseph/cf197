#include<stdio.h>
#include<math.h>
int main()
{
	float a, b, c, d, r1, r2,re, ip,rp;
	
	printf("Enter a,b and c \n");
	scanf("%f %f %f",&a,&b,&c);
	
	d=(b*b)-(4*a*c);
	if(d>0)
	{
		r1=(-b+sqrt(d)/(2*a));
		r2=(-b-sqrt(d)/(2*a));
		printf("Roots are real and distinct: %.2f and %.2f ", r1,r2);             
	}
	else if(d==0)
	{
		re=-b/(2*a);
		printf ("Roots are real and equal: %.2f " ,re);
	}
	else if(d<0)
	{
		rp=-b/(2*a);
		ip=sqrt(-d)/(2*a);
		printf("Roots are complex: %.2f-%.2f and %.2f+%.2f" , rp,ip,rp,ip);
	}
	
	return 0;
}

