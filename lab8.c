#include<stdio.h>

void input();
void output();

struct employee
{
    int Eid;
    char Ename[15];
    char DOJ[10];
    int salary;
};

int main()
{
    struct employee E;
    input(E);
    output(E);
    return 0;
}

void input( struct employee E)
{
    printf("\nEnter employee id ");
    scanf("%d",&E.Eid);
    printf("\nEnter employee name ");
    scanf("%s",&E.Ename);
    printf("\nEnter Date of Joining ");
    scanf("%s",&E.DOJ);
    printf("\nEnter salary ");
    scanf("%d",&E.salary);
}

void output( struct employee E)
{
    printf("\nEmployee id is %d ",E.Eid);
    printf("\nEmployee name is %s ",E.Ename);
    printf("\nDate of Joining is %s ",E.DOJ);
    printf("\nSalary is %d ",E.salary);
}