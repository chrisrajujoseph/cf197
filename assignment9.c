#include<stdio.h>
struct book:
{
    char title[20];
    char author[20];
    float price;
    int pages;
}

void input(struct book b1, struct book b2);
void output(struct book b1, struct book b2);

int main()
{
    struct book b1, b2;
    input(b1,b2);
    output(b1,b2);
    return 0;
}

void input(struct book b1, struct book b2)
{
    printf("\nEnter details (Title, Author, Price, no. of pages) \n\n");
    
    printf("\nEnter details of book 1 \n");
    scanf("%s%s%f%d",b1.title,b1.author,&b1.price,&b1.pages);
    
    printf("\nEnter details of book 2 \n");
    scanf("%s%s%f%d",b2.title,b2.author,&b2.price,&b2.pages);
}

void output(struct book b1,struct book b2)
{
    if(b1.price>b2.price)
        printf("\n%s is more expensive ",b1.title);
    else
        printf("\n%s is more expensive ",b2.title);
        
    if(b1.pages<b2.pages)
        printf("\n%s's book is shorter' ",b1.author);
    else
        printf("\n%s's book is shorter' ",b2.author);
}
